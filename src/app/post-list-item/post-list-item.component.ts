import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-post-list-item',
  templateUrl: './post-list-item.component.html',
  styleUrls: ['./post-list-item.component.scss']
})
export class PostListItemComponent implements OnInit {

	@Input () postTitle: string;
	@Input () postContent: string;
	@Input () postLike: number;
	@Input () postDislike: number;
	@Input () postCreatedAt: Date;


  constructor() { } // ce qui génère le component

  ngOnInit() {
  }

  onLoveIts() {
  	this.postLike++;
  }

  onDislike() {
  	this.postDislike++;
  }

}
