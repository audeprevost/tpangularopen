import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'Bienvenue dans mon Blog sur Angular';
    presentation = "Bonne visite !";

  posts = [
  	{
  		title: 'Mon premier post',
  		content: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. In fermentum ut odio congue lacinia. Aenean cursus tincidunt odio, a vestibulum turpis dignissim at. Donec non congue urna, et iaculis orci. Vivamus posuere quis odio non commodo. Nullam non augue ac mauris euismod euismod.' ,
  		like: 0,
  		dislike: 0,
  		createdAt: new Date(2018, 11, 5)
  	},

  	{
  		title: 'Mon deuxième post',
  		content: 'Aenean cursus tincidunt odio, a vestibulum turpis dignissim at.',
 		  like: 72,
  		dislike: 123,
  		createdAt: new Date(2019, 1, 18)
  	},

  	{
  		title: 'Encore un post',
  		content: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean cursus tincidunt odio, a vestibulum turpis dignissim at.',
 		  like: 51,
  		dislike: 24,
  		createdAt: new Date(2019, 3, 1)
  	}
  ];
}
